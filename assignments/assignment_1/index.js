
const FIRST_NAME = "Patricia";
const LAST_NAME = "Dragoi";
const GRUPA = "1077";

/**
 * Make the implementation here
 */
function initCaching() {
    var cache={};
    cache.pageAccessCounter=function(sectiuneSite='home'){
        sectiuneSite=new String(sectiuneSite).toLowerCase();
        if(cache.hasOwnProperty(sectiuneSite)){
            cache[sectiuneSite]++;
        }
        else{
            Object.defineProperty(cache,sectiuneSite,{
                value:1,
                writable:true
            });
        }
    }
    cache.getCache=function(){
        return this;
    }
    return cache;   
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}